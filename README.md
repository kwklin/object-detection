# Object Detection

This project performs human detection by training a pretrained model from Tensorflow. This project is also part of a bigger project called KeroBot, which is intended to build an actual robot that follows people around. However, not much of KeroBot hasn't been done but I'll continue working on it.

## v0

In this project, I first used the tools from https://github.com/tzutalin/labelImg to label 300 images of humans with different postures, such as sitting, standing, jumping, and so on. After that, I trained my model using the pretrained model SSD MobileNet V2 FPNLite 320x320 from Tensorflow Model Zoo and tested the model, achieving around 50% for both precision and recall rate. 

![False Positive](Pictures/wrong_1.PNG "False Positive")

One thing I noticed was that the model did a lot better on detecting a standing human compared to a human of any other posture. For example, it couldn't recognize the sitting human in a picture, but it would wrongly detects a human in a picture of a small fridge. For the next steps of this project, I would focus on images of standing human first and if time permits, I would put more time into training the data as well as using more images, so that the model could be more generalized and more humans of different posture could be detected.

## v1
Because the main goal of KeroBot is to build a robot that follows people around, I shifted my focus to pictures on standing/walking human and retrained the model. The model now achieves precision rates ranging from 67% to 100%. For my next step, I will add in the feature of real time detection.

![True Positive](Pictures/correct_1.PNG "True Positive")

